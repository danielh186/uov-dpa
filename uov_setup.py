import chipwhisperer as cw
import numpy as np
import pandas as pd

def main():
    """
    @brief      Measuring power consumtion of cw during run

    @details    COnnecting to a chipwhisperer

    @param      

    @return     
    """
    scope = cw.scope()

    # Set target type (copy of gen_reftraces.py 58 .. 67)
    try:
        if SS_VER == "SS_VER_2_1":
            target_type = cw.targets.SimpleSerial2
        elif SS_VER == "SS_VER_2_0":
            raise OSError("SS_VER_2_0 is deprecated. Use SS_VER_2_1")
        else:
            target_type = cw.targets.SimpleSerial
    except:
        SS_VER="SS_VER_1_1"
        target_type = cw.targets.SimpleSerial

    # Assign target
    try:
        target = cw.target(scope, target_type)
    except:
        print("ERROR: Could not connect! USB connection has died!")

if __name__ == "__main__":
    main()
