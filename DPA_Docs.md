# Figures

## 0xC4 sub 0xFF
The figure in the file with the same name shows the difference between *C4* and *FF*.
My guess is, that maybe the set bits maybe are not the ones with high spikes, but the ones with very similar scale.

If this is true, it would be possible to generate the needed reference trace for each bit by just using the *FF* Vector as comparisation.

## Bit positions

The positions of the bits are always at approximatly the same time index.
This means, that only about 400 / 1200 measurements (50 for each bit) are important, maybe getting rid of them would give a different insight using means and extremes.
