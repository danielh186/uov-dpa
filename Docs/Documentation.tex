\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{fancyhdr}
\usepackage{makeidx}
\usepackage[straightvoltages]{circuitikz}
\usepackage[locale=DE]{siunitx}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows.meta, positioning}
\usepackage{graphicx}
\usepackage{lmodern}
\usepackage{float}
\usepackage{cancel}
\usepackage[left=1cm, right=2cm, top=3cm, bottom=3cm, bindingoffset=1cm]{geometry}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{interval}
\usepackage{hyperref}
\author{Yannick Reiß, Daniel Hahn}
\title{DPA auf UOV --- Dokumentation}

\lstset{basicstyle=\ttfamily\small, language={[x86masm]Assembler}, keywordstyle=\color{red}\bfseries, numbers=left}

\begin{document}
\input{~/latex/init.tex}

\section{Der Algorithmus und seine Schwachstelle}

\subsection{Unbalanced Oil and Vinegar}
Der \textit{Oil and Vinegar}-Algorithmus ist ein Post-Quantum-Algorithmus
zur Erstellung von Signaturen und MACs mit Fokus auf Kollisionsresistenz und Pre-Image-Widerstand.\\
Dabei ist \textit{Oil} eine Menge geheimer Zufallszahlen und \textit{Vinegar} eine Menge mathematischer Transformationen, welche auf die Oil-Werte angewendet werden,
wie in der nachfolgenden Abbildung zu sehen.\\\\
% TODO N24: Add image showing the oav concept on Mon 14 Aug 2023 18:04:37 CEST by nick
Der \textit{Unbalanced Oil and Vinegar}-Algorithmus ist eine Variante des UOV-Algorithmus,
bei der die Mengengröße der Vinegar-Werten deutlich größer ist,
als die Mengengröße der Oil-Werte.\\\\
Die mögliche Post-Quantum Sicherheit besteht in der Verwendung von \textbf{überbestimmten multivariaten Polynomgleichungssystemen}, für welche bisher kein effizientes Lösungsverfahren bekannt ist.\\\\
Die hier genutzte Implementierung des UOV ist die Implementierung aus dem Paper\\
\href{https://eprint.iacr.org/2023/059}{\underline{\textcolor{blue}{Oil and Vinegar: Modern Parameters and Implementations}}}.

\subsection{Attacke auf UOV}
Die DPA-Attacke beruht,
wie die CPA-Attacke aus
\href{https://eprint.iacr.org/2023/335}{\underline{\textcolor{blue}{Seperating Oil and Vinegar with a Single Trace}}},
auf einer Schwachstelle im Code, über welche die Vinegar-Werte berechnet werden können.
Das Ziel unserer Attacke ist die Ermittlung genau dieser Vinegar-Werte mittels \textit{Differential Power Analysis}.
Im
\href{https://eprint.iacr.org/2023/335}{\underline{\textcolor{blue}{Paper}}}
wurde der geheime Schlüssel dann mittels der \textit{Kipnis-Shamir Attacke} und einer \textit{Reconsiliation-Attacke} berechnet.
Diesen Schritt werden wir nicht erneut gehen, da dieser Teil der Attacke unverändert bleibt.\\\\
In der nachfolgenden Abbildung ist die Angriffsstrategie veranschaulicht.
% TODO N70: Add image using the attack on Mon 14 Aug 2023 18:03:31 CEST by nick

\subsubsection{Aufbau der DPA}
Die DPA basiert auf nur einem Oil-Wert, wodurch deutlich weniger Informationen zur Verfügung stehen, als bei der CPA.\\
Zur Bestimmung der Peaks werden folgende Informationen verwendet:
\begin{itemize}
    \item  Position der Peaks
	\item  Höhe der Peaks
    \item  Höhe und Vorhandensein kleinerer Peaks vor den großen
    \item  Relatives Verhältnis dieser Kennwerte zueinander
\end{itemize}
Die Peaks in dem Diagramm werden erzeugt, indem man einen beliebigen Trace mit dem Trace für \( Vinegar = 0  \)subtrahiert.\\
\begin{figure}[H]
\centering
\includegraphics[width=0.8\linewidth]{../Figures/FF_sub_00.png}
    \caption{Differenz von FF als Input zu 0}
\end{figure}
Der oben abgebildete Trace ist der Referenz-Trace von FF (\( 11111111_{2} \)).
Hier sind alle 8 Bits gesetzt, die Ausschläge, sowie die vorhergehenden Peaks sind deutlich sichtbar.

\subsection{Die Schwachstelle}
\framebox{
\colorbox{blue!20}{
\begin{minipage}{1.0\textwidth}
\textbf{Anmerkung: Listings}\\
In nachfolgenden Code-Listings gilt:\\
Assembler-Code wurde aus dem C-Code des
\href{https://github.com/pqov/pqov-paper}{\underline{\textcolor{blue}{Repository zum Originalpaper}}}
mit dem ARM GCC 10.3.0 kompiliert.\\
C-Code verbleibt unverändert.
\end{minipage}}}

\subsubsection{ARM und MAU}
Die meisten ARM-Prozessoren (z. B. Cortex-M4) besitzen eine \textit{Multiply-Accumulate Unit} (MAC Unit / MAU),
welche bei unter anderem Multiplikationen genutzt wird.
Im Fall einer Null-Multiplikation optimiert die MAU die Multiplikation, um den Stromverbrauch zu reduzieren.
Dadurch weißt der Stromverbrauch einen Unterschied auf zwischen einer Multiplikation mit 0,
oder einer Multiplikation mit einer Zahl ungleich 0.

\subsubsection{Die Schwachstelle im Code}
In der Funktion \textbf{gf256v\_mul\_u32} wird die MAU des Prozessors abhängig vom Vinegar-Wert optimiert, oder nicht optimiert genutzt.
\begin{lstlisting}[language=C++]
r32 ^= (a32) * ((b32 >> 1) & 1);
\end{lstlisting}
Wenn für diese Zeile Code gilt, der Wert für \( a32 = 0 \), oder \( b32_{1} = 1 \) ,
dann erkennt die MAU, dass einer der Operanden Null ist und optimiert die Operation.

\subsubsection{Assembler-Code der gefährdenden Code-Zeile}
\begin{lstlisting}
        ldrh    r3, [r7, #12]
        lsrs    r3, r3, #1
        uxth    r3, r3
        and     r3, r3, #1
        uxth    r3, r3
        ldrh    r2, [r7, #14]   @ movhi
        smulbb  r3, r2, r3
        uxth    r3, r3
        sxth    r2, r3
        ldrsh   r3, [r7, #10]
        eors    r3, r3, r2
        sxth    r3, r3
        strh    r3, [r7, #10]   @ movhi
        ldrh    r3, [r7, #14]   @ movhi
        bic     r3, r3, #32512
        bic     r3, r3, #127
        strh    r3, [r7, #8]    @ movhi
\end{lstlisting}
Die entscheidende Zeile in diesem Listing ist die Zeile 7, welche eine Multiplikationsoperation ausführt.
Hier ist entscheidend, ob in Register 2 und 3 zum Zeitpunkt des Aufrufs eine Null steht.\\
Wenn \((A \land B) \), ist der Stromverbrauch durch die MAU wegen der Optimierung, bzw. weil keine Optimierung möglich ist, erhöht im Vergleich zu den anderen 3 möglichen Zuständen.

\subsection{Auswirkung der MAU-Operation auf Stromverbrauch über Zeit}
\begin{figure}[h]
\centering
\includegraphics[width=1.0\linewidth]{img/Sum5.png}
\caption{Beispielvektor für Differenz. Jeweils 5 Werte wurden hier summiert.}
\end{figure}
In Abbildung 2 ist die Differenz der Messung mit \( a = 0x0 \) und \( a = 0xb0 \) abgebildet.
Zur besseren Sichtbarkeit des Verlaufs wurden Intervalle der Länge 5 aufsummiert.\\
Bei dem ersten gesetzten  Bit ist ein deutlicher Ausschlag zu erkennen,
da die Optimierung des Stromverbrauchs jetzt wegfällt.

\paragraph{Problem in der Messung: falsch positive Ausschläge}
Die angezeigte Binärzahl ist (in Messfolge, also MSB rechtsseitig) \( 00001101 \),
wobei in der Grafik an der Stelle des 7. Bits ein Ausschlag zu erkennen ist, obwohl eigentlich keiner vorhanden sein dürfte.
Das Erkennen des ersten Bits ist ohne Probleme möglich,
da alle vorherigen Bits noch keinen Ausschlag produzieren.

\paragraph{Vermutung: Die MAU erhöht dauerhaft den Stromverbrauch}
\begin{itemize}
    \item  Muss zutreffen, da MAU die einzige Komponente mit Einfluss auf Stromverbrauch ist
	\item  Der genaue Effekt hängt von der MAU ab
	\item  Genauere Informationen über MAU des STM32F303 sind nicht verfügbar
\end{itemize}
Wenn der genaue Aufbau der verwendeten MAC Unit bekannt wäre,
könnte es möglich sein genauere Aussagen über die Wirkung auf den Stromverbrauch zu treffen.

\end{document}
