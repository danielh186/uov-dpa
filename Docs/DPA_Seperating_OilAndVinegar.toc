\babel@toc {german}{}\relax 
\beamer@sectionintoc {1}{Oil and Vinegar}{2}{0}{1}
\beamer@sectionintoc {2}{CPA-Attacke auf UOV}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Implementation des UOV Algorithmus}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Schwachstelle im Code}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Die Attacke}{7}{0}{2}
\beamer@sectionintoc {3}{DPA-Attacke auf UOV}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Ansatz für den Angriff}{9}{0}{3}
\beamer@subsectionintoc {3}{2}{Unterschied zur CPA}{10}{0}{3}
\beamer@subsectionintoc {3}{3}{Ansatz der Erkennung über Summe}{11}{0}{3}
\beamer@subsectionintoc {3}{4}{Ansatz über ein Neuronales Netz}{12}{0}{3}
\beamer@subsectionintoc {3}{5}{Ansatz über lokale Extrempunkte}{13}{0}{3}
