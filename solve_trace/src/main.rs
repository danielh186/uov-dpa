use std::io::prelude::*;
use std::{error::Error, io, process};
use std::fs::File;

fn main() {
    let zero_reference: &str = "0x0";
    let attack: &str = "0xff";
    let references_raw: Vec<f64> = get_references(zero_reference, attack, 1);
    let references: Vec<f64> = shorten(references_raw.clone(), 20);

    let mut pos_bits: Vec<usize> = Vec::new();
    let mut peaks: Vec<f64> = Vec::new();
    let mut higher: i32 = 9;
    let mut border: f64 = 0.0;

    // determine border
    while higher > 8 {
        higher = 0;
        println!("Now testing border {:?}", border);
        for reference in references.clone() {
            if reference > border {
                higher = higher + 1;
            }

            if higher > 8 {
                break;
            }
        }
        if higher <= 8 {
            break;
        }
        border = border + 0.0001;
    }

    for i in 0..references.len() {
        if references[i] > border {
            pos_bits.push(i);
            peaks.push(references[i]);
        }
    }

    println!("Sizes: {}, {}", references.len(), references_raw.len());
    println!("Found border {:?}", border);
    println!("Peaks: {:?}", pos_bits);
    println!("Values: {:?}", peaks);
}

fn get_references(zero: &str, attack: &str, _cat: i32) -> Vec<f64> {
    let file_zero = File::open(( String::from("../prepared_reftraces/reftrace_") + zero + ".csv" ).to_string()).expect("ERROR: Could not open file with zero trace.");
    let file_attack = File::open(( String::from("../prepared_reftraces/reftrace_") + attack + ".csv"  ).to_string()).expect("ERROR: Could not open file with attack trace.");

    let mut trace_zero = csv::ReaderBuilder::new().has_headers(false).from_reader(file_zero);

    let mut trace_attack = csv::ReaderBuilder::new().has_headers(false).from_reader(file_attack);

    let mut data_zero: Vec<f64> = Vec::new();
    let mut data_div: Vec<f64> = Vec::new();

    let mut index = 0;

    for result in trace_zero.records() {
        let record = result.expect("ERROR: Could not assign record.");
        for field in record.iter() {
            data_zero.push(field.parse::<f64>().expect("ERROR: Could not parse data!"));
            break;
        }

        if index < 1100 {
            index += 1;
        } else {
            break;
        }
    }

    let mut i = 0;
    let mut index = 0;
    for result in trace_attack.records() {
        let record = result.expect("ERROR: Could not assign record.");
        for field in record.iter() {
            data_div.push(field.parse::<f64>().expect("ERROR: Could not parse div data!") - data_zero[i]);
            break;
        }

        if index < 1100 {
            index += 1;
        } else {
            break;
        }

        i = i + 1;
    }
    data_div
}

// Vec<f64> shorten
// Shorten by summing up n numbers
fn shorten(prep: Vec<f64>, n: u32)  ->  Vec<f64> {
    let mut short: Vec<f64> = Vec::new();
    let mut sum: f64 = 0.0;
    let mut i: u32 = 0;
    for val in prep {
        sum += val;
        i += 1;

        if i >= n {
            short.push(sum);           
            i = 0;
            sum = 0.0;
        }
    }
    
    return short;
}

