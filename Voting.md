# Voting zur Bestimmung des Vektors

## Benötigte Daten

- 2 A-Traces mit je bis zu 
- 1100 Messungen und 
- $$ 2k+1 $$ B-Traces
- FF-A-Trace und
- $$ 2k+1 $$FF-B-Traces

## Aufbereitung der Daten

1. Probe-Traces und FF-Traces werden eingelesen
2. Beide werden um Faktor n zusammengefasst $$ SUM(i, i+n) $$ 
3. Von beiden wird der 0-Trace abgezogen 
4. Alle negativen Werte werden durch 0 ersetzt: $$ f(x) = (x > 0)? x : 0 $$ 
5. Probe-Traces werden von FF-Traces abgezogen: $$ FF-Probe $$ 
6. Gilt $$ FF-Probe < 0 $$, ist das Bit (vermutlich) gesetz, sonst (vermutlich) nicht
7. Angenommen wird der Wert, der durch mehr Traces angenommen wird

## Grafisches Beispiel

![Figures:Voting.png](./Figures/voting.png "Voting durch Traces")

## Algorithmus zur Bestimmung

Der Algorithmus entspricht der aktuellen Fassung von plot_traces.py.
Die Implementation der Abstimmung sieht folgenderweise aus:

```python
def voting(traces, voting_points):
    """
    Let traces vote for vector.

    Args:
        traces, voting_points ([[num]], [(num,num)]): Numpy array.

    Returns:
        vector The vector what is voted for.
    """

    for i in range( len( traces ) ):
        for j in range( len( traces[i] ) ):
            if traces[i][j] < -0.001:
                traces[i][j] = -1
            elif traces[i][j] > 0.001:
                traces[i][j] = 1
            else:
                traces[i][j] = 0 # just for clarification
   
    vector = []
    for p in voting_points:
        if np.array( traces[p[0]:p[1]] ).sum() > 1:
            vector.append(1)
        elif np.array( traces[p[0]:p[1]] ).sum() < -1:
            vector.append(0)
        else:
            vector.append('?')

    print(f"Voting with {len(traces)}.")

    return vector
```
**ACHTUNG: Bisher nur grafisch für die unten beschriebenen Fälle mit den angegebenen Genauigkeiten gültig! Der Code enthält Fehler!**

### Anmerkungen zur Votingfunktion

- Die Traces werden auf eindeutige Werte gesetzt, um zu verhindern, dass Ausreißer doppelte Stimmen erhalten
- Die Intervalle müssen für jedes neue Komprimierungsverhältnis angepasst werden
- Die Abstimmung geht *unentschieden* aus, wenn ein Bit im Bereich (-1, 1) liegt (bei Fehlschlag sind die möglichen falschen Bits schon bekannt)

## Trefferquote und Feinabstimmung

Für die folgenden Parameter ergab sich folgende Trefferquote durch Voting (ausgelesen aus Grafik):

|Datenpunkte|Intervalllänge|verbliebene Datenpunkte||unentschieden|fehler|
|---|---|---|---|---|---|
|1050|21|50||2|1|
|1034|22|47||1|1|
|1081|23|47||1|1|

Das obige Beispiel beschreibt den dritten dieser Fälle.
