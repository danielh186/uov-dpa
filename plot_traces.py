import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

view = (23, 1081) # 1081 / 23 = 47.0

def get_frame(filename: str):
    data_frame = pd.read_csv(filename)
    data_array = np.array(data_frame)

    return (data_frame, data_array)


def plot_data(data: np.array, data2: np.array):
    # for i in range(0,  data.shape[0]):
    #     plt.plot([j for j in range(data.shape[0])],  data[:, i])
    #plt.plot([j for j in range(data.shape[0])], data, "-b", lw=0.5)
    #plt.plot([k for k in range(data2.shape[0])], data2, "-y", lw=0.5)
    ref_data: np.array
    ref_data2: np.array
    const_sum: int = int( view[0] )
    ref1: list = []
    ref2: list = []
    tmp1: int
    tmp2: int
    for i in range(0, data.shape[0], const_sum):
        tmp1 = data[i:i+const_sum].mean()
        tmp2 = data2[i:i+const_sum].mean()
        
        ref1.append(tmp1)
        ref2.append(tmp2)
        #ref_data = np.absolute(np.array(ref1))
        #ref_data2 = np.absolute(np.array(ref2))
        ref_data = np.array(ref1)
        ref_data2 = np.array(ref2)

        plot_data = ref_data - ref_data2
        plot_data[plot_data < 0] = 0
    # plt.plot([l for l in range(int(data.shape[0] / const_sum))], plot_data, lw=0.7)

    return plot_data

def guess_bits(data_input):
    bits: np.array = np.zeros(8)
    dlength: int  = data_input.shape[0]
    border: int   = 0

    # Implement guessing criteria here
    for i in range(8):
        if data_input[int((dlength / 8) * i)] > border:
            bits[i] = 1

    # plot and return
    bits_extended: np.array = np.array(data_input)
    for i in range(8):
        bits_extended[(i * 138) : (i * 138 + 138)] = bits[i]
    plt.plot([i for i in range(int(data_input.shape[0]))], bits_extended)
    return bits


def main(file1: str, file2: str):
    frame, array = get_frame(f"prepared_reftraces/reftrace_{file1.replace('X', 'x')}.csv")
    frame2, array2 = get_frame(f"prepared_reftraces/reftrace_{file2.replace('X', 'x')}.csv")
    print(f"RUN {file1} VS {file2}")
    print(array.shape)
    print(array2.shape)
    print(array[:1100,1].shape)
    print("########################################")
    temp: list = []
    
    for i in range(1,26):
        temp.append(plot_data(array[:view[1],i], array2[:view[1],i]))
    return temp


def voting(traces, voting_points):
    """
    Let traces vote for vector.

    Args:
        traces, voting_points ([[num]], [(num,num)]): Numpy array.

    Returns:
        vector The vector what is voted for.
    """

    for i in range( len( traces ) ):
        for j in range( len( traces[i] ) ):
            if traces[i][j] < -0.001:
                traces[i][j] = -1
            elif traces[i][j] > 0.001:
                traces[i][j] = 1
            else:
                traces[i][j] = 0 # just for clarification
   
    vector = []
    for p in voting_points:
        if np.array( traces[p[0]:p[1]] ).sum() > 1:
            vector.append(1)
        elif np.array( traces[p[0]:p[1]] ).sum() < -1:
            vector.append(0)
        else:
            vector.append('?')

    print(f"Voting with {len(traces)}.")

    return vector


for i in range(1):
    num_string: str = "0x0"
    num2string: str = "0x15"
    references = main(num_string, num2string)
    
    num_string: str = hex(0)
    num2string: str = "0xff"
    temp = main(num_string, num2string)

    traces = []

    for i in range(0, len(temp)):
        t = references[i] - temp[i]
        traces.append(t)
        plt.plot([i for i in range(int(len(t)))], t)

    points = [
        (0, 6),
        (6, 12),
        (12, 18),
        (18, 24),
        (24, 29),
        (29, 35),
        (35, 41),
        (41, 47)]
    vector = voting(traces, points)
    print(vector)

plt.xlabel("measurement")
plt.ylabel("measured value")
plt.grid()
plt.xkcd(True)

plt.get_current_fig_manager().full_screen_toggle()

plt.show()
    
