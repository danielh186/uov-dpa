import tensorflow as tf
import numpy as np
import pandas as pd

def sub(fid1, fid2, b):
    file1: str = f"prepared_reftraces/reftrace_{fid1}.csv"
    file2: str = f"prepared_reftraces/reftrace_{fid2}.csv"

    frame1 = pd.read_csv(file1)
    frame2 = pd.read_csv(file2)

    array1 = np.array(frame1)
    array2 = np.array(frame2)

    array1 = array1[:1200, b]
    array2 = array2[:1200, b]

    return array2 - array1    

# Set the input and output sizes
input_size = 1200
output_size = 8

# Generate inputs based on first rows of the files
references: list = []
for i in range(256):
    num_string: str = "0x0"
    num_string2: str = hex(i)
    for i in range(42):
        references.append(sub(num_string, num_string2, i))
    
train_inputs = np.array(references)

# Generate outputs based on counting 0 to 255
bin_array: list = []
for i in range(256):
    temp_str: str = bin(i)[2:].zfill(8)
    temp_bin: list = [int(temp_str[i]) for i in range(8)]
    for i in range(42):
        bin_array.append(temp_bin)
train_outputs = np.array(bin_array)

## Random Ideas
## Use Average_Pooling to emphasize focus on the spikes

# Define the model
model = tf.keras.models.Sequential([
    tf.keras.layers.Dense(64, activation='relu', input_shape=(input_size,)),
    tf.keras.layers.Add(), # Added this to mix up the samples
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(1, activation='relu')
])

# Compile the model
model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.summary()

# Train the model
batch_size = 32
epochs = 400

model.fit(train_inputs, train_outputs, batch_size=batch_size, epochs=epochs, verbose=1)

model.save("V60.h5")

# Make predictions
test_inputs = np.random.rand(10, input_size)  # Generate random test inputs
predictions = model.predict(test_inputs)

print("Predictions:")
print(predictions)
